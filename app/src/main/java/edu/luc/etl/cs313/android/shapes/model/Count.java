package edu.luc.etl.cs313.android.shapes.model;

/**
 * A visitor to compute the number of basic shapes in a (possibly complex)
 * shape.
 */
public class Count implements Visitor<Integer> {

	// TODO entirely your job

	@Override
	public Integer onPolygon(final Polygon p) {

		return 1;
	}

	@Override
	public Integer onCircle(final Circle c) {
		Circle circle = new Circle(c.getRadius());

		if (circle.getRadius() == c.getRadius()){
			return 1;
		}
		else {
			return -1;
		}

	}

	@Override
	public Integer onGroup(final Group g) {
		int totalGroup = 0;

		for (Shape shape: g.getShapes()){
			totalGroup += shape.accept(this);
		}

		return totalGroup;
	}

	@Override
	public Integer onRectangle(final Rectangle q) {

		Rectangle rectangle = new Rectangle(q.getWidth(), q.getHeight());

		if (rectangle.getHeight() == q.getHeight() && rectangle.getWidth() == q.getWidth()){
			return 1;
		}
		else{
			return -1;
		}
	}

	@Override
	public Integer onOutline(final Outline o) {

		Shape outline = o.getShape();
		return outline.accept(this);
	}

	@Override
	public Integer onFill(final Fill c) {

		Shape fill = c.getShape();
		return fill.accept(this);
	}

	@Override
	public Integer onLocation(final Location l) {

		Shape location = l.getShape();
		return location.accept(this);
	}

	@Override
	public Integer onStroke(final Stroke c) {

		Shape Stroke = c.getShape();
		return Stroke.accept(this);
	}
}
